﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using dash;
using dash.Controllers;
using System.Net.Http;
using System.Web.Http;
using dash.Models;
using Moq;
using dash.Repositories;
using System.Collections.Generic;
using System;
using System.Web.Http.Results;
using System.Data.Entity;
using System.Linq;

namespace dash.Tests.Controllers
{
    [TestClass]
    public class PerformanceRepositoryTest
    {
        [TestMethod]
        public void GetBySecurityCode()
        {
            var msft = new Security
            {
                SecurityId = 1,
                SecurityCode = "MSFT"
            };

            var aapl = new Security
            {
                SecurityId = 2,
                SecurityCode = "AAPL"
            };

            var data = new List<PositionArchive>
            {
                new PositionArchive { Security = msft, DateId = 20160201, TotalPNL = 2000000m },
                new PositionArchive { Security = msft, DateId = 20160202, TotalPNL = 4312000m },
                new PositionArchive { Security = msft, DateId = 20160203, TotalPNL = -43000m },
                new PositionArchive { Security = msft, DateId = 20160204, TotalPNL = 10000m },
                new PositionArchive { Security = aapl, DateId = 20160201, TotalPNL = 15400000m },
                new PositionArchive { Security = aapl, DateId = 20160202, TotalPNL = 8760m },
                new PositionArchive { Security = aapl, DateId = 20160203, TotalPNL = 940m },
                new PositionArchive { Security = aapl, DateId = 20160204, TotalPNL = -64563m }
            };

            var set = new Mock<DbSet<PositionArchive>>()
                .SetupData(data);

            var context = new Mock<CoreContext>();
            context.Setup(a => a.PositionArchives).Returns(set.Object);

            var repo = new PerformanceRepository(context.Object);
            var result = repo.GetBySecurityCode("MSFT");
            
            Assert.AreEqual("MSFT", result.Identifier);
            Assert.AreEqual(data.Where(d => d.Security.SecurityCode == "MSFT").Sum(d => d.TotalPNL), result.Series.Sum(p => p.Value));
        }
        
    }
}
