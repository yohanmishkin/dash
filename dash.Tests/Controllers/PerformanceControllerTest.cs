﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using dash;
using dash.Controllers;
using System.Net.Http;
using System.Web.Http;
using dash.Models;
using Moq;
using dash.Repositories;
using System.Collections.Generic;
using System;
using System.Web.Http.Results;

namespace dash.Tests.Controllers
{
    [TestClass]
    public class PerformanceControllerTest
    {
        private Mock<IPerformanceRepository> _mockRepo = new Mock<IPerformanceRepository>();
        private PerformanceController _controller;

        public PerformanceControllerTest()
        {
            _controller = new PerformanceController(_mockRepo.Object);
        }

        [TestMethod]
        public void GetReturnsPerformance()
        {
            _mockRepo.Setup(x => x.GetBySecurityCode("MSFT"))
                .Returns(new Performance
                {
                    Identifier = "MSFT",
                    Series = new Dictionary<DateTime, decimal>
                    {
                        { new DateTime(2016, 02, 01), 0.53m },
                        { new DateTime(2016, 02, 02), 0.13m },
                        { new DateTime(2016, 02, 03), -0.23m }
                    }
                });

            var response = _controller.Get("MSFT");
            var contentResponse = response as OkNegotiatedContentResult<Performance>;

            Assert.IsNotNull(contentResponse);
            Assert.IsNotNull(contentResponse.Content);
            Assert.AreEqual("MSFT", contentResponse.Content.Identifier);
        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            var actionResult = _controller.Get("MSFT");

            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
    }
}
