﻿using dash.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace dash.Repositories
{
    public class PerformanceRepository : IPerformanceRepository
    {
        private readonly CoreContext _core;

        public PerformanceRepository()
        {
            _core = new CoreContext();
        }
        public PerformanceRepository(CoreContext core)
        {
            _core = core;
        }

        public Performance GetBySecurityCode(String securityCode)
        {
            var archives = _core.PositionArchives.Take(15).Where(p => p.Security.SecurityCode == securityCode);
            return new Performance
            {
                Identifier = securityCode,
                Series = archives.GroupBy(a => a.DateId)
                    .ToDictionary(x => 
                        DateTime.ParseExact(x.Key.ToString(), "yyyymmdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                        , y => y.Sum(a => a.TotalPNL))
            };
        }
    }
}