﻿using dash.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dash.Repositories
{
    public interface IPerformanceRepository
    {
        Performance GetBySecurityCode(String securityCode);
    }
}
