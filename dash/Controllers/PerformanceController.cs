﻿using dash.Models;
using dash.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace dash.Controllers
{
    public class PerformanceController : ApiController
    {
        private readonly IPerformanceRepository _repo;

        public PerformanceController()
        {
            _repo = new PerformanceRepository();
        }
        public PerformanceController(IPerformanceRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public IHttpActionResult Get(string securityCode)
        {
            Performance item = _repo.GetBySecurityCode(securityCode);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }
    }
}
