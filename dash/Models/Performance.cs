﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dash.Models
{
    public class Performance
    {
        public String Identifier { get; set; }
        public Dictionary<DateTime, Decimal> Series { get; set; }
    }
}